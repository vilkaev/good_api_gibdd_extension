// поменять активность галочки


document.getElementsByName("inputs").forEach(function (elem, i, arr) {

    elem.addEventListener("change", function change() {
        chrome.runtime.sendMessage({ action: this.id, value: this.checked, body: "changeAction" });
    });
});

function documentLod() {
    chrome.runtime.sendMessage({body:"getActionsFlags"}, function(response){

        console.log(JSON.stringify(response));
        for (var i=0; i<response.actions.length;i++){
            var action = response.actions[i];
            document.getElementById(action.name).checked = action.value;
        };
    });
};

document.addEventListener("DOMContentLoaded", documentLod());