var actors = {
    fines: {action: "fines", url:"https://xn--90adear.xn--p1ai/check/fines", title:"Проверка штрафов ГИБДД", tabID: 0, active:false},
    driver :{action:"driver", url: "https://xn--90adear.xn--p1ai/check/driver", title:"Проверка водителя", tabID: 0, active:false},
    car: {action:"car", url:"https://xn--90adear.xn--p1ai/check/auto", title:"Проверка автомобиля", tabID:0, active:false},
}

var backgroundCheckInterval = 10000;

function runTokenizer (actor) {
    
    chrome.tabs.create({ url: actor.url }, function (tab) {
        actor.active = true;
    });
};

// запустить на инициализации
// по дефолту водитель и штрафы
runTokenizer(actors.driver);
runTokenizer(actors.fines);

// чек-ин процедура для нового окна
function CheckINTab (action, url, body, senderID){

    if (actors[action].active==false)
    {
        console.log("action disabled");
        return "ActionDisabled";
    }

    if (actors[action].tabID==0)
    {
        // если активный таб пустой, приглашаем
        console.log("активация по пустом табу");
        return "BeActive";
    } 
    else if (senderID==actors[action].tabID)
    {
        // активная страница просит чек ин, значит - была перегружена
        console.log("активация по перегрузке");
        return "BeActive";
    }
    else 
    {
        var actTab = chrome.tabs.query({title:actors[action].title}, function(tabs) {
            if (tabs.lengh==0){
                console.log("активация по пустому массиву строк урл");
                return "BeActive"
            } else if (tabs.lengh==1)
            {
                console.log("активация -всего 1 страница с целевым урл");
                return "BeActive"
            } 
            else 
            {
                var activeFound =false;
                for (var i=0; i<tabs.lengh; i++){

                    if (tabs[i].id==actors[action].tabID
                        && tabId[i].url.startsWith(url))
                        {
                            activeFound=true;
                        }
                }
                
                if (!activeFound){
                    console.log("активация -всего не найдена страница с целевым урл");
                    return "BeActive";
                }
                else {
                    return "BeSecond";
                }
            }
        });
    }
}

function changeActionEvent(request){

    console.log(JSON.stringify(request));

    if (actors[request.action].active!=request.value)
    {
        actors[request.action].active=request.value;
        processActorsAcivity(actors[request.action]);
    } 
};

function processActorsAcivity(actor){

    if (actor.active==false)
    {
        if (actor.tabID!=0){
            console.log("удаление вкладки")
            chrome.tabs.remove(actor.tabID);
        }
    }
    else {
        if (actor.tabID==0){
            runTokenizer(actor);
        }
    };
}

// получить сообщение от контент-скрипта
chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        
        console.log(JSON.stringify(request));

        if (request.body=="changeAction")
        {
            changeActionEvent(request);
            return;

        } else if (request.body=="getActionsFlags"){
            
            // pop up запросил статус флагов
            var obj = {actions: [
                {name:"fines", value:actors.fines.active},
                {name:"driver", value:actors.driver.active},
                {name:"car", value:actors.car.active}
            ]
            };

            sendResponse(obj);
            return;
        }

        var senderURL = sender.tab.url;
        var senderID = sender.tab.id;

        console.log("msg get from client:"  +JSON.stringify(request));
        // регистрируем новый таб.
        if (request.body="check-in")
        {
            var result = CheckINTab(request.source.action, request.source.url, request.body, senderID);

            // указываем ид активного таба
            if (result=="BeActive"){
                actors[request.source.action].tabID = senderID;
            }
            // сообщаем ему самому.
            sendResponse({tabStatus: result});
        } 
        
});


// // регулярно проверяем, если ли на активных табах работник с нужным URL.
// // если нет - запускаем. работник связываем с этим бэком и сообщаем о себе. бэк сравнием ИД с текущим и либо делает работника активным, либо игнорит его. 
// var interval = 10000;
function regularCheckTabs(actor){

    console.log("регулярная проверка по урл:" + actor.url);
    chrome.tabs.query({ title: actor.title }, function (tabs) {
        if (tabs.length == 0) {
            console.log("dbg: окно с урл активног таба потеряно. запуск нового" + actor.url);
            runTokenizer(actor);
        }
        else {
            // проверим есть ли среди них активный
            var foundActive=false;
            console.log('dbg: поиск перебором активного' + actor.tabID);
            for (var i = 0; i < tabs.length; i++) {
                if (tabs[i].id==actor.tabID)
                    foundActive = true;
            }
            // активного среди них нет. 
            // устанавливаем активным первый
            if (foundActive==false){
                actor.tabID = tabs[0].id;
                chrome.tabs.sendMessage(tabs[0].id, {tabStatus: "BeActive"});
            }
        }
    });
}

setInterval(() => {

     if (actors.fines.tabID!=0 && actors.fines.active)
     {
         regularCheckTabs(actors.fines);
     };

     if (actors.driver.tabID!=0 && actors.driver.active) 
     {
        regularCheckTabs(actors.driver);
     }
     
     if (actors.car.tabID!=0 && actors.car.active)
        regularCheckTabs(actors.car);
     }

, backgroundCheckInterval);


function restartActor(actor) {
    actor.tabID==0;
    regularCheckTabs(actor);
}

// при удалении очищаем и запускаем процедуру назначения активного.
chrome.tabs.onRemoved.addListener(function (tabId, removeInfo) {
    
    if (actors.fines.tabID==tabId && actors.fines.active)
    restartActor(actor.fines);
    else if (actors.driver.tabID == tabId && actors.driver.active)
    restartActor(actors.driver)
    else if (actors.car.tabID == tabId && actors.car.active)
    restartActor(actors.car)
});
    

// уточнить
// возможноая проблема - неуникальный tab.ID


