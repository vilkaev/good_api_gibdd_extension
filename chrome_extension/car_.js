
console.log("запуск основного скрипта _vu_car");

//websocket
// let socket = new WebSocket("ws://localhost:57716/ws");
// let socket = new WebSocket("ws://localhost:64175/ws");

var timerID = 0;
var history = "auto_history";
var wanted = "auto_wanted";
var restricted = "auto_restricted";

function startSocket() {

    let socket = new WebSocket("wss://good-api.ru/ws");

    var tokensTimeOutInterval = 10000;
    var tokenPassed = 0;
    var startStatus = "start:"+ action_title;
    var readyStatus ="ready";
    var busyStatus = "busy";
    
    var myStatus ="";

    socket.onopen = function (e) {

        if (window.timerID){
            window.clearInterval(window.timerID);
            window.timerID =0;
        }

        console.log('open ws. sending ready: '+startStatus);
        socket.send(startStatus);
        myStatus = readyStatus;
    };


    socket.onmessage = function (event) {

        if (event.data == "needToken") {
            if (myStatus == readyStatus) {

                myStatus = busyStatus;

                // do captcha request
                grecaptcha.execute(reCaptchaSiteKey, { action: 'check_' + action_title }).then(function (token) {
                    appDriverCheck.reCaptchaToken = token;

                    console.log(`#tokenizer:${appDriverCheck.reCaptchaToken}`);
                    socket.send(`token:${appDriverCheck.reCaptchaToken}`);

                    // уведомить о готовности 
                    setTimeout(() => {
                        myStatus = readyStatus;
                        socket.send(myStatus);
                     },tokensTimeOutInterval);
                     
                });
            }
            else {
                socket.send(myStatus);
            }
        }
        else {
            console.log(`not parsed message ${event.data}`)
        }
    };

    socket.onclose = function (event) {
        if (event.wasClean) {
            console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        } else {
            console.log('[close] Connection died');
        }
        
        if (!window.timerID){
            console.log("trying to reconnect")
            setTimeout(function(){
                socket = null;
                startSocket();
            }, 25000);
        }
            
    };

    socket.onerror = function (error) {
        console.log(`[error] ${error.message}`);
    };
}

startSocket();
